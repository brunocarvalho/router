<?php

namespace Router\Interfaces;

interface Collection {

  public function set($key, $value);

  public function replace(array $items);

  public function all();

  public function remove($key);

  public function clear();

  public function count();

  public function keys();

  public function get($key, $default = null);

  public function has($key);
  
}
