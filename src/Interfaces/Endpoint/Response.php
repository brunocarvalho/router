<?php

namespace Router\Interfaces\Endpoint;

interface Response {

  /**
   * @param mixed $data
   * @return $this
   */
  public function add($data);

  public function send($status = 200, array $headers = array());

  public function error($message, $code);
  
}
