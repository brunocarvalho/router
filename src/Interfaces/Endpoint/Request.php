<?php

namespace Router\Interfaces\Endpoint;

interface Request {

  public function all();

  public function get($key, $default = null);

  public function has($key);
  
}
