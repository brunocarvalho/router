<?php

namespace Router;

use Router\Http\Response;
use ReflectionMethod;
use Exception;

class Endpoint {
  private $reflection;
  private $params;

  public function __construct($class, $method, array $params = array()) {
    if (method_exists($class, $method) && is_callable(array($class, $method))) {
      $this->reflection = new ReflectionMethod($class, $method);
      $this->params = $params;
    } else {
      throw new Exception('some of the aliases you requested do not exist', 405);
    }
  }

  public function nameParams(Collection $collection, Collection $request) {
    $docs = explode('/', $collection->get('api')[0]);
    $counter = 0;
    foreach ($docs as $value) {
      if (strpos($value, ':') !== false) {
        $name = str_replace(':', '', $value);
        if (strlen($name) > 0 && array_key_exists($counter, $this->params)) {
          $name = explode(' ', $name);
          $request->set($name[0], $this->params[$counter]);
          $counter++;
        }
      }
    }
  }

  public function invoke(Collection $request, Response $response) {
    $docs = DocComments::toArray($this->reflection->getDocComment());
    if ($docs->has('api')) {
      $this->nameParams($docs, $request);
    }
    $class = $this->reflection->class;
    die(call_user_func_array(array(new $class(), $this->reflection->getName()), func_get_args()));
  }

}
