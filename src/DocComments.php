<?php

namespace Router;

class DocComments {

  /**
   * @param string $docComments
   * @return Collection
   */
  public static function toArray($docComments) {
    $annotations = new Collection();
    $pattern = '/@(?P<name>[A-Za-z_-]+)(?:[ \t]+(?P<value>.*?))?[ \t]*\r?$/m';
    $matches = array();
    if (preg_match_all($pattern, substr($docComments, 3, -2), $matches)) {
      $numMatches = count($matches[0]);
      $tmp = array();
      for ($i = 0; $i < $numMatches; ++$i) {
        $tmp[$matches['name'][$i]][] = $matches['value'][$i];
      }
      $annotations->replace($tmp);
    }
    return $annotations;
  }

}
