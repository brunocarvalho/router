<?php

namespace Router\Http;

use Router\Interfaces\Endpoint\Response as ResponseInterface;

class Response implements ResponseInterface {
  private $message = array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-Status',
    208 => 'Already Reported',
    226 => 'IM Used',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => '(Unused)',
    307 => 'Temporary Redirect',
    308 => 'Permanent Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    426 => 'Upgrade Required',
    428 => 'Precondition Required',
    429 => 'Too Many Requests',
    431 => 'Request Header Fields Too Large',
    451 => 'Unavailable For Legal Reasons',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    508 => 'Loop Detected',
    510 => 'Not Extended',
    511 => 'Network Authentication Required'
  );
  private $body;
  private $hasData = false;

  private function getStatusMessage($code) {
    $protocol = (isset($_server['SERVER_PROTOCOL']) ? $_server['SERVER_PROTOCOL'] : 'HTTP/1.1');
    return "{$protocol} {$code} {$this->message[$code]}";
  }

  public function setHeaders(array $headers) {
    foreach ($headers as $key => $value) {
      is_int($key) ? header($value) : header("{$key}: {$value}");
    }
  }

  private function removeHeader($key) {
    header_remove($key);
  }

  public function add($data) {
    if (is_array($data) and is_array($this->body)) {
      $this->body = array_merge($this->body, $data);
    } else {
      $this->body = $data;
    }
    $this->hasData = true;
    return $this;
  }

  public function send($status = 200, array $headers = array()) {
    $headers[] = $this->getStatusMessage($status);
    $headers['Content-Type'] = 'application/json; charset=utf-8';
    $headers['Cache-Control'] = 'no-cache';
    if (!headers_sent()) {
      $this->setHeaders($headers);
      $this->removeHeader('X-Powered-By');
    }
    if ($this->hasData === true) {
      die(json_encode($this->body, JSON_PRETTY_PRINT));
    }
  }

  public function error($message, $code) {
    $error = array('code' => $code, 'type' => $this->message[$code], 'message' => $message);
    $this->add(array('error' => $error))->send($code);
  }

}
