<?php

namespace Router\Http;

use Exception;
use Router\Collection;

class Request {

  private $server;

  public function __construct(array $_server = array()) {
    $data = (count($_server) === 0) ? filter_input_array(INPUT_SERVER) : $_server;
    $this->server = new Collection($data);
  }

  public function getUri() {
    if ($this->server->has('REQUEST_URI')) {
      return strtok($this->server->get('REQUEST_URI'), '?');
    }
    throw new Exception('unexpected uri format');
  }

  public function getMethod() {
    if ($this->server->has('REQUEST_METHOD') or $this->server->has('HTTP_X_HTTP_METHOD')) {
      $method = $this->server->has('REQUEST_METHOD') ? $this->server->get('REQUEST_METHOD') : $this->server->get('HTTP_X_HTTP_METHOD');
      if ($method !== 'OPTIONS') {
        return $method;
      } else {
        die;
      }
    }
    throw new Exception('unexpected method format');
  }

  private function input($filename) {
    $inputHandler = fopen('php://input', 'r');
    $fileHandler = fopen($filename, 'w+');
    while (true) {
      $buffer = fgets($inputHandler, 4096);
      if (strlen($buffer) == 0) {
        fclose($inputHandler);
        fclose($fileHandler);
        return true;
      }
      fwrite($fileHandler, $buffer);
    }
  }

  public function getValues() {
    $values = [];
    $filename = sys_get_temp_dir() . "/data.bin";
    if($this->input($filename)){
      $input = file_get_contents($filename);
      if (is_array($temp = explode('&', $input))) {
        foreach ($temp as $item) {
          $element = explode('=', $item);
          if (array_key_exists(0, $element) && array_key_exists(1, $element)) {
            $values[$element[0]] = $element[1];
          }
        }
      }
      if (is_array($temp = json_decode($input, true))) {
        $values = array_merge($values, $temp);
      }
    }
    if (count($temp = filter_input_array(INPUT_GET)) > 0) {
      $values = array_merge($values, $temp);
    }
    return new Collection($values);
  }
  
  public function getHeaders() {
    $results = new Collection();
    foreach ($this->server->all() as $key => $value) {
      if (substr($key, 0, 5) == 'HTTP_') {
        $key = str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($key, 5))));
        $results->set($key, $value);
      } else if ($key == "CONTENT_TYPE") {
        $results->set(strtolower($key), $value);
      } else if ($key == "CONTENT_LENGTH") {
        $results->set(strtolower($key), $value);
      }
    }
    return $results;
  }

}
