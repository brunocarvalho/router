<?php

namespace Router;

use Exception;
use Router\Http\Request,
    Router\Http\Response;

class Middleware {
  private $configurations;
  private $request;
  private $response;

  public function __construct(array $configurations = array()) {
    $this->configurations = new Collection($configurations);
    $this->request = new Request();
    $this->response = new Response();
  }

  public function run() {
    if ($this->configurations->has('headers')) {
      $this->response->setHeaders($this->configurations->get('headers'));
    }
    try {
      $uri = array_filter(explode('/', $this->configurations->has('uri') ? $this->configurations->get('uri') : $this->request->getUri()));
      foreach ($uri as $index => $path) {
        $tmp = array_slice($uri, 0, $index);
        foreach ($tmp as $key => $value) {
          $tmp[$key] = (is_numeric($value)) ? 'V' . str_replace('.', '', $value) : ucfirst(strtolower($value));
        }
        array_unshift($tmp, $this->configurations->get('namespace'));
        if (class_exists($class = implode('\\', $tmp))) {
          $endpoint = new Endpoint($class, $this->request->getMethod(), array_slice($uri, $index));
          $endpoint->invoke($this->request->getValues(), $this->response);
        }
      }
      throw new Exception('some of the aliases you requested do not exist', 404);
    } catch (Exception $error) {
      $this->response->error($error->getMessage(), $error->getCode());
    }
  }

}
