<?php

namespace Router;

use Router\Interfaces\Collection as CollectionInterface,
    Router\Interfaces\Endpoint\Request as RequestInterface;

class Collection implements CollectionInterface, RequestInterface {
  private $data = array();

  public function __construct(array $items = array()) {
    $this->replace($items);
  }

  public function set($key, $value) {
    $this->data[$key] = $value;
  }

  public function replace(array $items) {
    foreach ($items as $key => $value) {
      $this->set($key, $value);
    }
  }

  public function all() {
    return $this->data;
  }

  public function remove($key) {
    unset($this->data[$key]);
  }

  public function clear() {
    $this->data = array();
  }

  public function count() {
    return count($this->data);
  }

  public function keys() {
    return array_keys($this->data);
  }

  public function get($key, $default = null) {
    return $this->has($key) ? $this->data[$key] : $default;
  }

  public function has($key) {
    return array_key_exists($key, $this->data);
  }

}
