Router
======

this is a php-toolkit that helps you quickly write simple yet powerful APIs

## Usage

```php
<?php

require_once __DIR__ . '/vendor/autoload.php';

(new Router\Middleware([
  'namespace' => 'Module\\Routes',
  'headers' => [
    'Access-Control-Allow-Origin' => '*',
    'Access-Control-Allow-Methods' => '*',
    'Access-Control-Allow-Headers' => 'X-Requested-With, content-type, Authorization'
  ]
]))->run();
```

if use apache
```apacheconf
RewriteEngine On
Options +FollowSymLinks
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [L]
```
if use php cli
```sh
$ php -S 0.0.0.0:8080 index.php
```